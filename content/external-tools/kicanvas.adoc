+++
title = "KiCanvas"
toolimage = "/img/external-tools/kicanvas.png"
tooldeveloper = "theacodes "
toolurl = "https://kicanvas.org/"
+++

KiCanvas is an interactive, browser-based viewer for KiCAD schematics and boards.
It is written in modern vanilla TypeScript and uses the Canvas element and WebGL for rendering.
