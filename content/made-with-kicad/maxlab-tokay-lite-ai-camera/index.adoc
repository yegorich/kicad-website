+++
title = "Tokay Lite: ESP32 Edge AI Camera"
projectdeveloper = "MAXLAB.IO"
projecturl = "https://github.com/maxlab-io/tokay-lite-pcb"
"made-with-kicad/categories" = [
    "Development Board",
    "USB Device",
    "Wireless"
]
+++

link:https://maxlab.io/store/edge-ai-camera/[Tokay Lite] is the ESP32 camera
development board ideal for low-power image processing applications on the edge.

Tokay Lite can be used as standalone devkit to aid developers with a convinient
tool or be embedded in the bigger system to augument it with the vision and
image processing data.
